const gulp = require('gulp'),
	stylus = require('gulp-stylus'),
	minifyCss = require('gulp-minify-css'),
	autoprefixer = require('gulp-autoprefixer'),
	htmlmin = require('gulp-htmlmin'),
	uglify = require('gulp-uglify'),
	babel = require('gulp-babel'),
	browserSync = require('browser-sync').create(),
	imagemin = require('gulp-imagemin'),
	plumber = require('gulp-plumber'),
	concat = require('gulp-concat'),
	svgSprite = require('gulp-svg-sprite'),
	pug = require('gulp-pug'),
	pump = require('pump');

/* Paths */
const srcPath = 'src';
const src = {
	styl: `${srcPath}/styl/`,
	html: `${srcPath}/**/*.html`,
	js: `${srcPath}/js/`,
	img: `${srcPath}/img/`,
	fonts: `${srcPath}/fonts/**/*.*`
};

const destPath = 'dist';
const dest = {
	css: `${destPath}/css`,
	html: destPath,
	js: `${destPath}/js`,
	img: `${destPath}/img`,
	fonts: `${destPath}/fonts/`
};

/* Tasks */

// Styles
gulp.task('stylus', () => {
	pump([
		gulp.src(src.styl + 'style.styl'),
		plumber(err => console.error(err)),
		stylus({
			compress: false
		}),
		autoprefixer({ browsers: ['last 2 versions'] }),
		minifyCss(),
		gulp.dest(dest.css),
		browserSync.stream({ match: '**/*.css' })
	]);
});

// HTML
gulp.task('html', () => {
	pump([
		gulp.src(src.html),
		plumber(err => console.error(err)),
		// pug(),
		htmlmin({ collapseWhitespace: true, removeComments: true }),
		gulp.dest(dest.html)
	]);
});

// JS
gulp.task('js', () => {
	pump([
		gulp.src([
			`${src.js}cryptoStocks.js`,
			`${src.js}spriteCanvas.js`,
			`${src.js}app.js`
		]),
		plumber(err => console.error(err)),
		concat('bundle.js'),
		babel({ presets: ['es2015', 'es2017'] }),
		uglify(),
		gulp.dest(dest.js),
		browserSync.stream()
	]);
});

// Place fonts
gulp.task('fonts', function() {
	return gulp.src(src.fonts).pipe(gulp.dest(dest.fonts));
});

// SVG Sprite
gulp.task('svg', () => {
	return gulp
		.src(src.img + 'icons/*.svg')
		.pipe(
			svgSprite({
				mode: {
					symbol: {
						prefix: '.',
						sprite: '../dist/img/icon-sprite.svg',
						inline: true,
						render: {
							styl: {
								dest: '../src/styl/sprite.styl',
								template: src.styl + 'templates/_sprite.styl'
							}
						}
					}
				}
			})
		)
		.on('error', error => {
			throw new Error(error);
		})
		.pipe(gulp.dest('.'));
});

// Image minify
gulp.task('image', () => {
	pump([
		gulp.src(src.img),
		plumber(err => console.error(err)),
		imagemin({ verbose: true }),
		gulp.dest(dest.img)
	]);
});

// BrowserSync
gulp.task('browserSync', ['fonts', 'stylus', 'js', 'html'], () => {
	browserSync.init({
		injectChanges: true,
		server: `./${destPath}`
	});
	gulp.watch(`${src.styl}**/*.styl`, ['stylus']);
	gulp.watch(`${src.js}**/*.js`, ['js']);
	gulp.watch(src.html, ['html']);
	gulp.watch(src.html).on('change', browserSync.reload);
	gulp.watch(src.styl).on('change', browserSync.reload);
	gulp.watch(src.js).on('change', browserSync.reload);
});

gulp.task('default', ['browserSync']);
