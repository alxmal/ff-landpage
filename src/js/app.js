playSpriteAnim();
window.onload = () => {
	let loaderWrapper = document.querySelector('.loader-wrap');
	let loader = document.querySelector('.loader');
	let introFade = document.querySelector('.intro-textblock');
	setTimeout(() => {
		loader.classList.add('loaded');
	}, 1200);

	setTimeout(() => {
		loaderWrapper.classList.add('loaded');
	}, 1300);

	setTimeout(() => {
		loaderWrapper.classList.add('done');
	}, 1800);

	setTimeout(() => {
		toggleClassDelayed(introFade, 'fade-in', 120)
	}, 1000);

};

function toggleClassDelayed(elem, cssClass, delay) {
	let count = 0;
	let delayTimer = setInterval(function() {
	  if (count < elem.children.length) {
		elem.children[count].classList.toggle(cssClass);
		count++;
	  } else {
		clearInterval(delayTimer);
	  }
	}, delay);
  }

/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
particlesJS.load('particles-js', '/assets/particlesjs-config.json', function() {
	console.log('callback - particles.js config loaded');
});
