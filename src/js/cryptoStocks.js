const stocksBlock = document.querySelector('.stocks');

const makeCryptoCards = (coinName, coinValue, coinChange, coinChangePct) => {
	let cryptoCard = document.createElement('div'),
		name = document.createElement('p'),
		value = document.createElement('p'),
		change = document.createElement('p'),
		changeIcon = document.createElement('span'),
		changeStyle;

	if (coinChangePct.charAt(0) === '-') {
		changeStyle = 'change-down';
		changeIcon.innerHTML =
			'<svg class="icon"><use xlink:href="/img/icon-sprite.svg#arrow-dropdown"></use></svg>';
	} else {
		coinChange = `+${coinChange}`;
		coinChangePct = `+${coinChangePct}`;
		changeStyle = 'change-up';
		changeIcon.innerHTML =
			'<svg class="icon"><use xlink:href="/img/icon-sprite.svg#arrow-dropup"></use></svg>';
	}

	cryptoCard.classList.add('currency');
	cryptoCard.setAttribute('data', coinName);
	name.textContent = coinName;
	name.classList.add('name');
	value.textContent = `${coinValue}`;
	value.classList.add('value');
	change.textContent = `${coinChange} (${coinChangePct}%)`;
	change.appendChild(changeIcon);
	change.classList.add(changeStyle);
	cryptoCard.appendChild(name);
	cryptoCard.appendChild(value);
	cryptoCard.appendChild(change);
	stocksBlock.appendChild(cryptoCard);
};

const getCryptoValues = () => {
	fetch(
		'https://min-api.cryptocompare.com/data/pricemultifull?fsyms=BTC,ETH,XRP,BCH,EOS,LTC&tsyms=USD&e=CCCAGG'
	)
		.then(response => {
			return response.json();
		})
		.then(data => {
			console.log(data.DISPLAY);
			Object.keys(data.DISPLAY).forEach(function(key) {
				let coinName = key.toString();
				let coinValue = data.DISPLAY[key].USD.PRICE;
				let coinChange = data.DISPLAY[key].USD.CHANGE24HOUR.slice(2);
				let coinChangePct = data.DISPLAY[key].USD.CHANGEPCT24HOUR;
				console.log(coinName);
				console.log(coinValue);
				console.log(coinChange);
				console.log(coinChangePct);
				makeCryptoCards(coinName, coinValue, coinChange, coinChangePct);
			});
		})
		.catch(error => {
			console.log(error);
		});
};

getCryptoValues();
