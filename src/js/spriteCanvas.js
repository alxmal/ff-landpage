(function() {
	let lastTime = 0;
	let vendors = ['ms', 'moz', 'webkit', 'o'];

	for (let x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
		window.requestAnimationFrame =
			window[vendors[x] + 'RequestAnimationFrame'];
		window.cancelAnimationFrame =
			window[vendors[x] + 'CancelAnimationFrame'] ||
			window[vendors[x] + 'CancelRequestAnimationFrame'];
	}

	if (!window.requestAnimationFrame)
		window.requestAnimationFrame = function(callback, element) {
			let currTime = new Date().getTime(),
				timeToCall = Math.max(0, 16 - (currTime - lastTime)),
				id = window.setTimeout(function() {
					callback(currTime + timeToCall);
				}, timeToCall);

			lastTime = currTime + timeToCall;
			return id;
		};

	if (!window.cancelAnimationFrame)
		window.cancelAnimationFrame = function(id) {
			clearTimeout(id);
		};
})();

const spriteCfg = {
	canvasWidth: 48,
	canvasHeight: 48,
	src: 'img/logo-sprite-v3-48p.png',
	spriteWidth: 384,
	spriteHeight: 48,
	numberOfFrames: 8,
	ticksPerFrame: 1
}

function playSpriteAnim() {
	class Sprite {
		constructor(options) {
			(this.obj = {}),
				(this.frameIndex = 0),
				(this.tickCount = 0),
				(this.ticksPerFrame = options.ticksPerFrame || 0),
				(this.numberOfFrames = options.numberOfFrames || 1);

			this.context = options.context;
			this.width = options.width;
			this.height = options.height;
			this.image = options.image;
		}

		update() {
			this.tickCount += 1;

			if (this.tickCount > this.ticksPerFrame) {
				this.tickCount = 0;
				// If the current frame index is in range
				if (this.frameIndex < this.numberOfFrames - 1) {
					// Go to the next frame
					this.frameIndex += 1;
				} else {
					this.frameIndex = 0;
				}
			}
		}

		render() {
			// Clear the canvas
			this.context.clearRect(0, 0, this.width, this.height);
			// Draw the animation
			this.context.drawImage(
				this.image,
				(this.frameIndex * this.width) / this.numberOfFrames,
				0,
				this.width / this.numberOfFrames,
				this.height,
				0,
				0,
				this.width / this.numberOfFrames,
				this.height
			);
		}
	}

	// Get canvas
	const canvas = document.getElementById('anim-canvas');
	canvas.width = spriteCfg.canvasWidth;
	canvas.height = spriteCfg.canvasHeight;

	// Create sprite sheet
	let spriteImage = new Image();

	// Load sprite sheet
	spriteImage.addEventListener('load', animloop);
	spriteImage.src = spriteCfg.src;

	// Create sprite
	let sprite = new Sprite({
		context: canvas.getContext('2d'),
		width: spriteCfg.spriteWidth,
		height: spriteCfg.spriteHeight,
		image: spriteImage,
		numberOfFrames: spriteCfg.numberOfFrames,
		ticksPerFrame: spriteCfg.ticksPerFrame
	});

	// Animation loop
	function animloop() {
		sprite.update();
		sprite.render();
		window.requestAnimationFrame(animloop);
	}
}
